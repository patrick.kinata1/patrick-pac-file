# PAC file

This is intended for personal use to set up PAC in mac so VPN is not mandatory.

## Setting up

1. Link this pac script to your mac on settings -> Wi-fi -> Proxies -> Automatic proxy configurations and input the URL to the pac file
2. Run one of this command in the terminal for staging and production respectively

```bash
( timeout 1800 tsh ssh -N -D 127.0.0.1:5551 ubuntu@s-id-em-kyc-service-patroni-pg-01 ) &
```

```bash
( timeout 1800 tsh ssh -N -D 127.0.0.1:5552 ubuntu@p-id-em-kyc-service-patroni-pg-01 ) &
```
