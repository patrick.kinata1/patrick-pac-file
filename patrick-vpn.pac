function FindProxyForURL(url, host) {
  if (shExpMatch(host, "*.i.s-id-em-01.gopay.sh")) {
    return "SOCKS5 127.0.0.1:5551";
  } else if (shExpMatch(host, "emoney-gopay-portal-staging.id.golabs.io")) {
    return "SOCKS5 127.0.0.1:5551";
  } else if (shExpMatch(host, "*.i.p-id-em-01.gopay.sh")) {
    return "SOCKS5 127.0.0.1:5552";
  }
  return "DIRECT";
}
